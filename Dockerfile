FROM nginx:1.18.0
RUN rm -rf /etc/nginx/conf.d
RUN mkdir -p /etc/nginx/conf.d
COPY nginx/default.conf /etc/nginx/conf.d/
COPY public/. /usr/share/nginx/html
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]

HEALTHCHECK --interval=5m --timeout=3s \
 CMD curl -f http://localhost:8080 || exit 1
